import re
score=0
with open("input1.txt") as file :
    line0 = file.readline()
    line1 = file.readline()
    file.readline()
    pair=1
    while line0 :
        line0 = line0.replace("[","[a").replace("]","a]").replace(",","a").replace("\n","").replace("aa","a").split("a")
        line1 = line1.replace("[","[a").replace("]","a]").replace(",","a").replace("\n","").replace("aa","a").split("a")
        for i in range(0,len(line0)+1):
            if line0[i] != line1[i] :
                if line0[i]=="]":
                    score+=pair
                    print(str(pair) + " Out of range ("+str(i)+")")
                    break
                elif line1[i]=="]":
                    print("out of range line1 ("+str(i)+")")
                    break
                elif line0[i]=="[":
                    line1.insert(i,"[")
                    line1.insert(i+2,"]")
                    print(line1)
                elif line1[i]=="[":
                    line0.insert(i,"[")
                    line0.insert(i+2,"]")
                    print(line0)
                elif int(line0[i]) > int(line1[i]) :
                    print(str(pair) + " sup0 ("+str(i)+")")
                    break
                elif int(line0[i]) < int(line1[i]) :
                    score+=pair
                    print(str(pair) + " sup1 ("+str(i)+")")
                    break
                else :
                    print("diff :"+line0[i]+"/"+line1[i]) 
            else :
                print("same :"+line0[i]+"/"+line1[i]) 


        line0 = file.readline()
        line1 = file.readline()
        file.readline()
        pair+=1

print(score)