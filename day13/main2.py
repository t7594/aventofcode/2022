import re
score=0
tab=[]
with open("input.txt") as file :
    tmp = file.readline()
    while tmp :
        tab.append(tmp.replace("\n",""))
        tab.append(file.readline().replace("\n",""))
        file.readline()
        tmp = file.readline()

tab.append("[[2]]")
tab.append("[[6]]")
    

switcher=True
tour=0
while switcher and tour < 500 :
    switcher = False
    nb_switch=0
    for j in range(0,len(tab)-1) :
        line0 = tab[j].replace("[","[a").replace("]","a]").replace(",","a").replace("aa","a").split("a")
        line1 = tab[j+1].replace("[","[a").replace("]","a]").replace(",","a").replace("\n","").replace("aa","a").split("a")
        switch = True
        for i in range(0,len(line0)+1):
            if line0[i] != line1[i] :
                if line0[i]=="]":
                    switch = False
                    break
                elif line1[i]=="]":
                    break
                elif line0[i]=="[":
                    line1.insert(i,"[")
                    line1.insert(i+2,"]")
                elif line1[i]=="[":
                    line0.insert(i,"[")
                    line0.insert(i+2,"]")
                elif int(line0[i]) > int(line1[i]) :
                    break
                elif int(line0[i]) < int(line1[i]) :
                    switch = False
                    break
                else :
                    print("diff :"+line0[i]+"/"+line1[i]) 
        
        if switch :
            switcher = True
            nb_switch+=1
            tab[j+1], tab[j] = tab[j], tab[j+1]

            if tour > 497 :
                print(tab[j])
                print(tab[j+1])
            
    print("tour : "+str(tour)+"/"+str(nb_switch))
    tour+=1


print(str(tab.index("[[2]]")) + "/" + str(tab.index("[[6]]")))
print((tab.index("[[2]]")+1)*(tab.index("[[6]]")+1))