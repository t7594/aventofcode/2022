import string

map_alpha = {}
for idx, x in enumerate (list(string.ascii_lowercase + string.ascii_uppercase)) :
    map_alpha[x] = idx + 1

score=0
with open("input.txt") as file :
    line0 = file.readline()
    line1 = file.readline()
    line2 = file.readline()
    while line0 :

        for c in list(line0):
            if line1.find(c) != -1 and line2.find(c) != -1 :
                score +=map_alpha[c]
                break
    
        line0 = file.readline()
        line1 = file.readline()
        line2 = file.readline()


print(score)