
score=0
with open("input.txt") as file :
    for line in file:
        tmp = line.replace("A","1").replace("B","2").replace("C","3").replace("X","1").replace("Y","2").replace("Z","3").split()
        a = int(tmp[0])
        
        c = int(tmp[1])
        if c==1 :
            b=a-1
            if b == 0:
                b=3
        elif c==2 :
            b=a 
        else :
            b=a+1
            if b == 4 :
                b=1

        score += b
        if a == b :
            score +=3
        elif ((a+1) == b) or (b==1 and a==3) :
            score +=6

print(score)